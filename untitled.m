%% testing deflation method

%% order of the polynomial
p = 10;
%% function \prod(x - a).. i = 1:n where a is a random number
a = randi([-100 100],1,p);

x0 = -10;
r = [a(1)];
fprintf('%dst root of function = %5.4f\n',1,a(1));

found_all_roots = 0;

ctr = 0;
max_it = 100;
converged = 0;

x = x0;

while(~found_all_roots)
    ctr = 0;
    max_it = 100;
    converged = 0;

    x = x0;
    rlen = length(r);

    while(ctr < max_it & ~converged)
        ctr = ctr + 1;
    
        %% Original function
        f = prod(x-a);
    
        %% Deflation operator
        M = 1;
        for(i=1:rlen)
            M = M*(x - r(i));
        end
    
        %% Deflated function
        f_hat = f/M;
    
        %% Originial Jacobian
        J = f/(x-a(1));
        for(i=2:p)
            temp = 1;
            for(j=1:p)
                if (i ~= j)
                    temp = temp*(x-a(j));
                end
            end
            J = J + temp;
        end
    
        Mprime = M/(x-r(1));
        for(i=2:rlen)
            temp = 1;
            for(j=1:rlen)
                if (i ~= j)
                    temp = temp*(x-r(j));
                end
            end
            Mprime = Mprime + temp;
        end
        
        J_hat = (M*J - f*Mprime)/M^2;
        if(abs(J_hat) < 1e-12)
            break;
        end
    
        dx = -f_hat/J_hat;
    
        x = x + dx;
    
        f = prod(x-a);
    
        %% Deflation operator
        M = 1;
        for(i=1:rlen)
            M = M*(x - r(i));
        end
    
        %% Deflated function
        f_hat = f/M;
    
        if(norm(f_hat) < 1e-8)
            converged = 1;
        end
    end
    
    if(converged)
        r = [r,x];
        idx = find(abs(x - a) < 1e-8);
        fprintf('Number of roots found = %d\nFound %dth root of function = %5.4f\n',length(r)-1,idx,x);
        
    else
        found_all_roots = 1;
    end
end