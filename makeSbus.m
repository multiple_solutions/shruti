function Sbus = makeSbus(baseMVA, bus, gen, mpopt)
%MAKESBUS   Builds the vector of complex bus power injections.
  %   SBUS = MAKESBUS(BASEMVA, BUS, GEN, MPOPT) returns the vector of complex bus
%   power injections, that is, generation minus load. Power is expressed
%   in per unit.
%
%   See also MAKEYBUS.

%   MATPOWER
%   Copyright (c) 1996-2015 by Power System Engineering Research Center (PSERC)
%   by Ray Zimmerman, PSERC Cornell
%
%  03/20/2015: Shrirang Abhyankar - Added one more input argument mpopt to support ZIP load model
%
%   $Id: makeSbus.m 2644 2015-03-11 19:34:22Z ray $
%
%   This file is part of MATPOWER.
%   Covered by the 3-clause BSD License (see LICENSE file for details).
%   See http://www.pserc.cornell.edu/matpower/ for more info.

%% define named indices into bus, gen matrices
[PQ, PV, REF, NONE, BUS_I, BUS_TYPE, PD, QD, GS, BS, BUS_AREA, VM, ...
    VA, BASE_KV, ZONE, VMAX, VMIN, LAM_P, LAM_Q, MU_VMAX, MU_VMIN] = idx_bus;
[GEN_BUS, PG, QG, QMAX, QMIN, VG, MBASE, GEN_STATUS, PMAX, PMIN, ...
    MU_PMAX, MU_PMIN, MU_QMAX, MU_QMIN, PC1, PC2, QC1MIN, QC1MAX, ...
    QC2MIN, QC2MAX, RAMP_AGC, RAMP_10, RAMP_30, RAMP_Q, APF] = idx_gen;

%% generator info
on = find(gen(:, GEN_STATUS) > 0);      %% which generators are on?
gbus = gen(on, GEN_BUS);                %% what buses are they at?

%% form net complex bus power injection vector
nb = size(bus, 1);
ngon = size(on, 1);
Cg = sparse(gbus, (1:ngon)', ones(ngon, 1), nb, ngon);  %% connection matrix
                                                        %% element i, j is 1 if
                                                        %% gen on(j) at bus i is ON
SbusG = Cg * (gen(on, PG) + 1j * gen(on, QG));      %% power injected by generators
zip_p = mpopt.pf.zip_pw/100.0;
zip_q = mpopt.pf.zip_qw/100.0;
Vm = bus(:,VM);
temp = [ones(nb,1) Vm Vm.^2];
SbusD = bus(:,PD).*(temp * zip_p') + 1j * bus(:,QD).*(temp * zip_q');
Sbus = (SbusG - SbusD) / baseMVA; %% Complex power injection converted to p.u.
