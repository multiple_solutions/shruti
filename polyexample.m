% x=-5:0.01:5;
% y=(x-1)*(x+1)*(x-10);
clear all;
% syms x;
Xinit=0;
itermax=100;iter=1;
xsol=Xinit;
M=1;
tol=10^-4;
forig=(xsol-1)*(xsol+1)*(xsol-10);
Jacob=3*xsol^2-18*xsol-12;%diff(forig);
for solnum=1:3
    while iter<=itermax && abs(forig)>tol
        f=M*forig;
        xsol=xsol-1/Jacob*(f);
        forig=(xsol-1)*(xsol+1)*(xsol-10);
        Jacob=polder(f);
        iter=iter+1;
    end
    if iter>itermax
        disp('fail to converge');
    else
        xsoli(solnum)=xsol;
        M=M/(Xinit-xsoli(solnum));
        iter=1;forig=10;xsol=Xinit;
    end
end