function [Sxfr] = makeSxfr(mpcbase, mpctarget, mpopt)
%MAKESXFR - Builds a vector of complex bus power transfers.
% SXFR = MAKESXFR(MPCBASE, MPCTARGET, MPOPT) returns the vector of
% complex bus power transfers, that is, Sbustarget - Sbusbase. Power
% is expressed in per unit.
%
% See also MAKESBUS

%   MATPOWER
%   Copyright (c) 1996-2015 by Power System Engineering Research Center (PSERC)
%   by Ray Zimmerman, PSERC Cornell
%
%  03/21/2015: Shrirang Abhyankar
%
%   $Id: makeSbus.m 2644 2015-03-11 19:34:22Z ray $
%
%   This file is part of MATPOWER.
%   Covered by the 3-clause BSD License (see LICENSE file for details).
%   See http://www.pserc.cornell.edu/matpower/ for more info.

%% define named indices into bus
[PQ, PV, REF, NONE, BUS_I, BUS_TYPE, PD, QD, GS, BS, BUS_AREA, VM, ...
    VA, BASE_KV, ZONE, VMAX, VMIN, LAM_P, LAM_Q, MU_VMAX, MU_VMIN] = idx_bus;

%% base case complex bus power injections (generation - load)
Sbusb = makeSbus(mpcbase.baseMVA, mpcbase.bus, mpcbase.gen,mpopt);
%% compute target case complex bus power injections (generation - load)
Sbust = makeSbus(mpctarget.baseMVA, mpctarget.bus, mpctarget.gen,mpopt);

%% scheduled transfer
Sxfr = Sbust - Sbusb;
