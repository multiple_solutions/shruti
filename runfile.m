clear;
close all;
global Vall max_num_sol alphsol powsol
% mpc=loadcase('case5loop');       % current balance form : get 4 solutions (2other form), compare both parts of current
mpc=loadcase('case14Yang');        %current balance form gets 2 solutions
% mpc=loadcase('case5_thorp2');    %current balance form gets two solutions
% mpc=loadcase('case7_thorp'); %current balance form gets 1 solutions
max_num_sol=100;
% max_num_sol=4;
mpct=mpc;
loop=1;
scalemin=1;%0;%
scalestep=0.1;finestep=0.01;
scalefin=1;%3.1;%
% scalefin=3.3;
scalerange=[scalemin:scalestep:scalefin];%,scalefin+finestep:finestep:scalefin+scalestep];
bus2allsol(1:max_num_sol,1:length(scalerange))=1000;bus3allsol(1:max_num_sol,1:length(scalerange))=1000;
bus4allsol(1:max_num_sol,1:length(scalerange))=1000;bus5allsol(1:max_num_sol,1:length(scalerange))=1000;
for scale=scalerange
    mpct.bus(:,3)=mpc.bus(:,3)*scale;    %real demand
    mpct.bus(:,4)=mpc.bus(:,4)*scale;    %reactive demand
    mpct.gen(:,2)=mpc.gen(:,2)*scale;    %real generation
    results=runpf(mpct,mpoption('pf.nr.max_it',200,'pf.tol', 1e-4));%,'pf.tol', 1e-4
    if ~isempty(Vall)
        numsol=length(Vall(2,:));
        numsolstore(loop)=numsol;
        bus2allsol(1:numsol,loop)=Vall(2,:);
        bus3allsol(1:numsol,loop)=Vall(3,:);
        bus4allsol(1:numsol,loop)=Vall(4,:);
        bus5allsol(1:numsol,loop)=Vall(5,:);
        scalefact(loop)=scale;
        loop=loop+1;
        numsol
    else
        break;
    end
%     pause;
end
loop=loop-1;
magsol=abs(Vall);
angsol=angle(Vall)*180/pi;
% plot(scalefact,numsolstore);
% title('Number of solutions found at different scaling factors');
% % mTextBox = uicontrol('style','text');
% % set(mTextBox,'String','Maximum number of solutions possible: 50');
% xlabel('Scaling factor');
% ylabel('Number of solutions');
% 
% endind=max(numsolstore);
% bus2allsol(endind+1:end,:)=[];bus3allsol(endind+1:end,:)=[];bus4allsol(endind+1:end,:)=[];bus5allsol(endind+1:end,:)=[];
% allsols=[bus2allsol;zeros(1,loop);bus3allsol;zeros(1,loop);bus4allsol;zeros(1,loop);bus5allsol];
% 
% angles2=angle(bus2allsol)*180/pi;angles3=angle(bus3allsol)*180/pi;angles4=angle(bus4allsol)*180/pi;angles5=angle(bus5allsol)*180/pi;
% voltsol=[bus2allsol(1:numsol),bus3allsol(1:numsol),bus4allsol(1:numsol),bus5allsol(1:numsol)];
% 
% figure();
% defl=sort(magsol(3,:),'descend');yang=sort(magyang(3,:),'descend');
% scatter(1:length(yang),yang);hold on;
% scatter(1:length(defl),defl);legend('Yang''s solutions','Deflation method');
% title('Bus 3 voltage magnitudes');
% 
% figure();
% deflang=sort(angsol(3,:),'descend');yangang=sort(angyang(3,:),'descend');
% scatter(1:length(yangang),yangang);hold on;
% scatter(1:length(deflang),deflang);legend('Yang''s solutions','Deflation method');
% title('Bus 3 voltage angles in degrees');
% 
% numless30def=length(find(deflang>=-30));
% numless30yang=length(find(yangang>=-30));
% 
% numbet4060def=length(find(-60<=deflang & deflang<=-40));
% numbet4060yang=length(find(-60<=yangang & yangang<=-40));
% 
% numbet120150def=length(find(-150<=deflang & deflang<=-120));
% numbet120150yang=length(find(-150<=yangang & yangang<=-120));
% 
% numbet160180def=length(find(-180<=deflang & deflang<=-160));
% numbet160180yang=length(find(-180<=yangang & yangang<=-160));
% 
% defbracnums=[numless30def;numbet4060def;numbet120150def;numbet160180def];
% yangbracnums=[numless30yang;numbet4060yang;numbet120150yang;numbet160180yang];
% figure();
% scatter(1:length(defbracnums),yangbracnums);hold on;
% scatter(1:length(defbracnums),defbracnums);legend('Yang''s solutions','Deflation method');
% title('Number of solutions in the 4 angle brackets');
% 
% sum(defbracnums)
% sum(yangbracnums)
% 
% 
% numlesspt7def=length(find(defl>=0.8));
% numlesspt7yang=length(find(yang>=0.8));
% 
% numbetpt7pt8def=length(find(0.7<=defl & defl<0.8));
% numbetpt7pt8yang=length(find(0.7<=yang & yang<0.8));
% 
% numbetpt5pt7def=length(find(0.5<=defl & defl<0.7));
% numbetpt5pt7yang=length(find(0.5<=yang & yang<0.7));
% 
% numbetlesspt2def=length(find(defl<=0.2));
% numbetlesspt2yang=length(find(yang<=0.2));
% 
% defbracnummag=[numlesspt7def;numbetpt7pt8def;numbetpt5pt7def;numbetlesspt2def];
% yangbracnummag=[numlesspt7yang;numbetpt7pt8yang;numbetpt5pt7yang;numbetlesspt2yang];
% figure();
% scatter(1:length(defbracnummag),yangbracnummag);hold on;
% scatter(1:length(defbracnummag),defbracnummag);legend('Yang''s solutions','Deflation method');
% title('Number of solutions in the 4 magnitude brackets');
% sum(defbracnummag)
% sum(yangbracnummag)
% % figure();
% % for i=1:loop
% %     angles=angle(bus5allsol(:,i))*180/pi;
% %     angles(angles==0)=[];
% %     [anglessorted,ind]=sort(angles);
% %     scatter(scalefact(i)*ones(numsolstore(i),1),anglessorted(1:numsolstore(i)));hold on;
% %     title('Angle of voltages at bus 5 for different load-scaling factors');
% % end