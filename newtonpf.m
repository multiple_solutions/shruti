function [V, converged, i] = newtonpf(Ybus, baseMVA, bus, gen, V0, ref, pv, pq, mpopt)
%NEWTONPF  Solves the power flow using a full Newton's method.
%   [V, CONVERGED, I] = NEWTONPF(YBUS, BASEMVA, BUS, GEN, V0, REF, PV, PQ, MPOPT)
%   solves for bus voltages given the full system admittance matrix (for
%   all buses), the initial vector of complex bus voltages, and column vectors with
%   the lists of bus indices for the swing bus, PV buses, and PQ buses,
%   respectively. The bus voltage vector contains the set point for
%   generator (including ref bus) buses, and the reference angle of the
%   swing bus, as well as an initial guess for remaining magnitudes and
%   angles. MPOPT is a MATPOWER options struct which can be used to 
%   set the termination tolerance, maximum number of iterations, and 
%   output options (see MPOPTION for details). Uses default options if
%   this parameter is not given. Returns the final complex voltages, a
%   flag which indicates whether it converged or not, and the number of
%   iterations performed.
%
%   See also RUNPF.

%   MATPOWER
%   Copyright (c) 1996-2015 by Power System Engineering Research Center (PSERC)
%   by Ray Zimmerman, PSERC Cornell
%
%   $Id: newtonpf.m 2644 2015-03-11 19:34:22Z ray $
%
%   This file is part of MATPOWER.
%   Covered by the 3-clause BSD License (see LICENSE file for details).
%   See http://www.pserc.cornell.edu/matpower/ for more info.
%
%  03/20/2015: Shrirang Abhyankar - Using input argument baseMVA, bus,gen instead of Sbus to support ZIP load model. Sbus needs to be calculated at each iteration as the loads could be voltage dependent.

[PQ, PV, REF, NONE, BUS_I, BUS_TYPE, PD, QD, GS, BS, BUS_AREA, VM, ...
    VA, BASE_KV, ZONE, VMAX, VMIN, LAM_P, LAM_Q, MU_VMAX, MU_VMIN] = idx_bus;

%% default arguments
if nargin < 7
    mpopt = mpoption;
end

nbus = size(bus,1);

%% options
tol     = mpopt.pf.tol;
max_it  = mpopt.pf.nr.max_it;


%% set up indexing for updating V
npv = length(pv);
npq = length(pq);
j1 = 1;         j2 = npv;           %% j1:j2 - V angle of pv buses
j3 = j2 + 1;    j4 = j2 + npq;      %% j3:j4 - V angle of pq buses
j5 = j4 + 1;    j6 = j4 + npq;      %% j5:j6 - V mag of pq buses

%% flag to run deflation loop 
deflation = 0;

%% array of PF solutions found
pfsols = [];
npfsols = 0;

max_def = 4;
%alpha = eye(length(j1:j6)) + 0.1*diag([bus([pv;pq],PD);bus(pq,QD)]/baseMVA);
pow = 2;
%damping = 0.5;
while(deflation < max_def)
    %% initialize
    converged = 0;
    i = 0;
    V = V0;
    Va = angle(V);
    Vm = abs(V);
    
    %% evaluate Sbus
    bus(:,VM) = Vm;
    Sbus = makeSbus(baseMVA,bus,gen,mpopt);

    %% evaluate F(x0)
    mis_orig = V .* conj(Ybus * V) - Sbus;
    
    %% deflation operator
    M = ones(nbus,1);
    for j=1:npfsols
       M([pv]) = M([pv]).*(Vm([pv]).*(exp(1j*(Va(pv))) - exp(1j*angle(pfsols([pv],j))))).^pow;
       M([pq]) = M([pq]).*(V([pq]) - pfsols([pq],j)).^pow;
    end
%     M = ones(nbus,1);
%     for j=1:npfsols
%        temp = norm(V - pfsols(:,j),2)^pow; 
%        M = M.*temp;
%     end
    
    F_orig = [   real(mis_orig([pv; pq]));
                 imag(mis_orig(pq))   ];
    %% deflated function
    mis = mis_orig./M;
    
    
    
    F = [   real(mis([pv; pq]));
                 imag(mis(pq))   ];
    
    %% check tolerance
    normF = norm(F, inf);
    if mpopt.verbose > 1
        fprintf('\n it    max P & Q mismatch (p.u.)');
        fprintf('\n----  ---------------------------');
        fprintf('\n%3d        %10.3e', i, normF);
    end
    if normF < tol
        converged = 1;
        if mpopt.verbose > 1
            fprintf('\nConverged!\n');
        end
    end

    %% do Newton iterations
    while (~converged && i < max_it)
        %% update iteration counter
        i = i + 1;
    
        %% evaluate Jacobian
        [dSbus_dVm, dSbus_dVa] = dSbus_dV(Ybus, V);
        [dSload_dVm, dSload_dVa] = dSload_dV(V, baseMVA, bus, issparse(Ybus),mpopt);
   
        dSbus_dVm = dSbus_dVm + dSload_dVm;
        dSbus_dVa = dSbus_dVa + dSload_dVa;

        j11 = real(dSbus_dVa([pv; pq], [pv; pq]));
        j12 = real(dSbus_dVm([pv; pq], pq));
        j21 = imag(dSbus_dVa(pq, [pv; pq]));
        j22 = imag(dSbus_dVm(pq, pq));
    
        Mprime = zeros(length(j1:j6));
        for j=1:npfsols
            temp = -pow*diag(([Va([pv;pq]);Vm(pq)] - [angle(pfsols([pv;pq],j));abs(pfsols(pq,j))]).^-(pow+1));
            for k=1:npfsols
                if (j ~= k)
                    temp = temp*diag(([Va([pv;pq]);Vm(pq)] - [angle(pfsols([pv;pq],k));abs(pfsols(pq,k))]).^-pow);
                end
            end
            Mprime = Mprime + temp;
        end
        
        Jorig = [   j11 j12;
                j21 j22;    ];
            
        J1 = Jorig; %(M*Jorig + Mprime*diag(F_orig));    

         %% Finite differenced Jacobian
%          delta = 1e-8;
%          
%          J1 = zeros(npv+2*npq,npv+2*npq);
%          idx1 = 1;
%          idx2 = 1;
%          idx3 = 1;
%          for s = 1:npv+2*npq
%              xa = Va;
%              xm = Vm;
%              if s <= npv
%                  xa(pv(idx1)) = xa(pv(idx1)) + delta;
%                  idx1 = idx1 + 1;
%              elseif s <= npv + npq
%                  xa(pq(idx2)) = xa(pq(idx2)) + delta;
%                  idx2 = idx2 + 1;
%              else
%                  xm(pq(idx3)) = xm(pq(idx3)) + delta;
%                  idx3 = idx3 + 1;
%              end
%              x = xm.*exp(1j*xa);
%              
%              
%              %% evaluate Sbus
%              bus(:,VM) = xm;
%              Sbus = makeSbus(baseMVA,bus,gen,mpopt);
% 
%             %% evaluate F(x)
%             mis_orig = x .* conj(Ybus * x) - Sbus;
%     
%             %% deflation operator
%             M = ones(nbus,1);
%             for j=1:npfsols
%                 M([pv]) = M([pv]).*(xm([pv]).*(exp(1j*(xa(pv))) - exp(1j*angle(pfsols([pv],j))))).^pow; 
%                 M([pq]) = M([pq]).*(x([pq]) - pfsols([pq],j)).^pow;
%             end
% %             M = ones(nbus,1);
% %             for j=1:npfsols
% %                 temp = norm(x - pfsols(:,j),2)^pow; 
% %                 M = M.*temp;
% %             end
%     
%             %% deflated function
%             mis = mis_orig./M;
%     
%             F1 = [   real(mis([pv; pq]));
%                      imag(mis(pq))   ];
%             
%             J1(:,s) = (F1 - F)/delta;
%             
%          end
         
        
        %% compute update step
        dx = -(J1 \ F);

        normFprv = normF;
        normF = normFprv + 1.0;
        Vaprv = Va;
        Vmprv = Vm;
        damping = 2.0;
        while(normF > normFprv && damping > 1e-4)
            damping = damping/2.0;
            %% update voltage
            if npv
                Va(pv) = Vaprv(pv) + damping*dx(j1:j2);
            end
            if npq
                Va(pq) = Vaprv(pq) + damping*dx(j3:j4);
                Vm(pq) = Vmprv(pq) + damping*dx(j5:j6);
            end
            V = Vm .* exp(1j * Va);
            Vm = abs(V);            %% update Vm and Va again in case
            Va = angle(V);          %% we wrapped around with a negative Vm

            %% evaluate Sbus
            bus(:,VM) = Vm;
            Sbus = makeSbus(baseMVA,bus,gen,mpopt);

            %% evaluate F(x0)
            mis_orig = V .* conj(Ybus * V) - Sbus;
            
            F_orig = [   real(mis_orig([pv; pq]));
                         imag(mis_orig(pq))   ];
    
            %% deflation operator
            M = ones(nbus,1);
            for j=1:npfsols
                M([pv]) = M([pv]).*(Vm([pv]).*(exp(1j*(Va(pv))) - exp(1j*angle(pfsols([pv],j))))).^pow;
                M([pq]) = M([pq]).*(V([pq]) - pfsols([pq],j)).^pow;
            end
%           M = ones(nbus,1);
%           for j=1:npfsols
%             temp = norm(V - pfsols(:,j),2)^pow; 
%             M = M.*temp;
%           end
    
            %% deflated function
            mis = mis_orig./M;
    
            F = [   real(mis([pv; pq]));
                    imag(mis(pq))   ];

            %% check for convergence
            normF = norm(F, inf);
        end
        if mpopt.verbose > 1
           fprintf('\n%3d        %10.3e', i, normF);
        end
        if normF < tol
           converged = 1;
           if mpopt.verbose
              fprintf('\nNewton''s method power flow converged in %d iterations.\n', i);
           end
        end
    end %% End of Newton loop
    
    if converged
        npfsols = npfsols + 1;
        pfsols = [pfsols,V];
        
           Vm0 = abs(V);
           Va0 = angle(V);
           Vm0(pq) = 0.01; %Vm0(pq) + 1e-3;
           Va0([pv;pq]) = Va0([pv;pq]) + 1e-3;
           V0 = Vm0.*exp(sqrt(-1)*Va0);
        
%         V0 = [
%         1.000000000000000 + 0.000000000000000i
%         0.980697737283908 - 0.195529916090121i
%         0.927831509038025 - 0.372999585571099i
%         0.638057821797498 - 0.125480483723282i
%         0.029457645005034 - 0.084011855987012i
%         0.716532303989737 - 0.341739043509334i
%         0.753767695046557 - 0.338885221302666i
%         0.842604530026727 - 0.271877238945942i
%         0.631344971073336 - 0.246317708338088i];
%     
%         Vm0 = Vm;
%         Vm0(pq) = abs(V0(pq)) + 0.02;
%         Va0 = angle(V0);
%         V0 = Vm0.*exp(sqrt(-1)*Va0);
        

        deflation = deflation + 1;
    end
    if mpopt.verbose
        if ~converged
            fprintf('\nNewton''s method power flow did not converge in %d iterations.\n', i);
            error('Did not converge\n');
            
        end
    end
end
