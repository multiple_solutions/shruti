%% testing deflation method

%% order of the polynomial
p = 2;

%% dimension of x
n = 4;

a = zeros(n,p);
%% function \prod(x - a).. i = 1:n where a is a random number
a(1:n/2,:) = 0.0+(1.1-0.0)*rand(n/2,p);
a(n/2+1:n,:) = -2*pi+(4*pi)*rand(n/2,p);
%a = randi([1 10],n,p);
%a =  [1 2 3;
%      2 3 4;
%      ];
  
x0 = [ones(n/2,1);zeros(n/2,1)];
r = a(:,1);
fprintf('%dst root of function\n',1);
disp(a(:,1));

found_all_roots = 0;

ctr = 0;
max_it = 100;
converged = 0;

x = x0;

while(~found_all_roots)
    ctr = 0;
    max_it = 100;
    converged = 0;

    x = x0;
    rlen = size(r,2);

    while(ctr < max_it & ~converged)
        ctr = ctr + 1;
    
        %% Original function
        f = x - a(:,1);
        for i = 2:p
            f = f.*(x - a(:,i));
        end
    
        %% Deflation operator
        M = eye(n);
        for(i=1:rlen)
            M = M*diag(1./(x - r(:,i)));
        end
    
        %% Deflated function
        f_hat = M*f;
    
        %% Originial Jacobian
        J = diag(f./(x-a(:,1)));
        for(i=2:p)
            temp = eye(n);
            for(j=1:p)
                if (i ~= j)
                    temp = temp*diag(x-a(:,j));
                end
            end
            J = J + temp;
        end
    
        Mprime = zeros(n);
        for(i=1:rlen)
            temp = diag(-(x - r(:,i)).^-2);
            for(j=1:rlen)
                if (i ~= j)
                    temp = temp*diag((x-r(:,j)).^-1);
                end
            end
            Mprime = Mprime + temp;
        end
        
        J_hat = (M*J + Mprime*diag(f));
        if(abs(trace(J_hat)) < 1e-12)
            break;
        end
    
        dx = -J_hat\f_hat;
    
        x = x + dx;
    
        %% Original function
        f = x - a(:,1);
        for i = 2:p
            f = f.*(x - a(:,i));
        end
    
        %% Deflation operator
        M = eye(n);
        for(i=1:rlen)
            M = M*diag(1./(x - r(:,i)));
        end
    
        %% Deflated function
        f_hat = M*f;
    
        if(norm(f_hat) < 1e-8)
            converged = 1;
        end
    end
    
    if(converged)
        r = [r,x];
        fprintf('%dth root of function\n',size(r,2));
        disp(x);
        
    else
        found_all_roots = 1;
    end
end