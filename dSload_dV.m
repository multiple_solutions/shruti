function [dSload_dVm, dSload_dVa] = dSload_dV(V,baseMVA, bus,sparseform,mpopt)
%DSBUS_DV   Computes partial derivatives of power injection w.r.t. voltage.
%   [DSLOAD_DVM, DSLOAD_DVA] = DSLOAD_DV(V, BASEMVA, BUS, SPARSEFORM, MPOPT) returns two matrices containing
%   partial derivatives of the complex load injections w.r.t voltage
%   magnitude and voltage angle respectively (for all buses). If sparseform is a
%   true, the return values will be sparse. The following explains
%   the expressions used to form the matrices for the ZIP load model where
%   the load injection is a function of the voltage magnitude only:
%
%   dPD_dVm = PD0 * ( [0 1 2*Vm] * zip_p')
%   dQD_dVm = QD0 * ( [0 1 2*Vm] * zip_q')
%   dSload_dVm = dPD_dVm + j * dQD_dVm
%
%   Example:
%       [dSload_dVm, dSload_dVa] = dSload_dV(V, baseMVA, bus, 1, mpopt);

%   MATPOWER
%   Copyright (c) 1996-2015 by Power System Engineering Research Center (PSERC)
%   by Ray Zimmerman, PSERC Cornell
%
%   $Id: dSbus_dV.m 2644 2015-03-11 19:34:22Z ray $
%
%   This file is part of MATPOWER.
%   Covered by the 3-clause BSD License (see LICENSE file for details).
%   See http://www.pserc.cornell.edu/matpower/ for more info.
%
%   03/21/2015: Shrirang Abhyankar

[PQ, PV, REF, NONE, BUS_I, BUS_TYPE, PD, QD, GS, BS, BUS_AREA, VM, ...
    VA, BASE_KV, ZONE, VMAX, VMIN, LAM_P, LAM_Q, MU_VMAX, MU_VMIN] = idx_bus;

n = length(V);
zip_p = mpopt.pf.zip_pw/100;
zip_q = mpopt.pf.zip_qw/100;
Vm = abs(V);

temp = [zeros(n,1), ones(n,1), 2*Vm];
dPD_dVm = bus(:,PD)/baseMVA .* (temp * zip_p');
dQD_dVm = bus(:,QD)/baseMVA .* (temp * zip_q');

if sparseform           %% sparse version (if Ybus is sparse)
    dSload_dVa  = sparse(n,n);
    dSload_dVm  = sparse(1:n,1:n,dPD_dVm+1j*dQD_dVm,n,n);
else                        %% dense version
    dSload_dVa  = zeros(n,n);
    dSload_dVm = diag(dPload_dVm + 1j*dQD_dVm);
end
