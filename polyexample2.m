clear all;close all;
syms x;
Xinit=5;
itermax=100;iter=1;
xsol=Xinit;
M=1;
tol=10^-5;
foriginal=(x-1)*(x+1)*(x);
forig=double(subs(foriginal,Xinit));
for solnum=1:3
    while iter<=itermax && abs(forig)>tol
        f=M*foriginal;
        jacobexp=diff(f);
        Jacob=double(subs(jacobexp,xsol));
        xsol=xsol-1/Jacob*(double(subs(f,xsol)));
        forig=double(subs(foriginal,xsol));
        iter=iter+1;
    end
    if iter>itermax
        disp('fail to converge');
    else
        xsoli(solnum)=xsol;
        M=M/(x-xsoli(solnum));
        Msol(solnum)=1/(x-xsoli(solnum));
        iter=1;forig=10;xsol=Xinit;
    end
end

xplot=[-2:0.01:-1.0001,-1.00000001,-0.99999999:0.01:-0.000001,0.000001:0.01:0.9999,0.999999,1.000001:0.01:1.99999999,2.00000001:0.01:2];
y=(xplot-1).*(xplot+1).*(xplot);
plot(xplot,y,'LineWidth',2);
Msolvec=double(subs(Msol(1),xplot));
ydef1=y.*Msolvec;%(xplot+1).*(xplot-10);%
hold on;
plot(xplot,ydef1,'LineWidth',2);
ydef2=ydef1.*double(subs(Msol(2),xplot));%(xplot+1);%
plot(xplot,ydef2,'LineWidth',2);
legend('Original function','Deflated function1','Deflated function 2');
hold on;
plot(xplot,zeros(length(xplot),1),'k','LineWidth',2);hold on;
plot(zeros(length(-100:100),1),-100:100,'k','LineWidth',2);
ylim([-10 10]);
% ylim([min(min(y),min(ydef1),min(ydef2)) max(max(y),max(ydef1),max(ydef2))]);