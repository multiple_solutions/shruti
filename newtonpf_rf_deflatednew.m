function [V, converged, ctr,Vall] = newtonpf_rf_deflatednew(Ybus, Sbus, V0, ref, pv, pq, mpopt)
%NEWTONPF  Solves the power flow using a full Newton's method with
%          i)   Equations for pq buses expressed in current balance form.
%          ii)  Voltages expressed in rectangular form
%          iii) Equations for each pq bus written in the format [f_imag;f_real]
%   [V, converged, i] = newtonpf(Ybus, Sbus, V0, ref, pv, pq, mpopt)
%   solves for bus voltages given the full system admittance matrix (for
%   all buses), the complex bus power injection vector (for all buses),
%   the initial vector of complex bus voltages, and column vectors with
%   the lists of bus indices for the swing bus, PV buses, and PQ buses,
%   respectively. The bus voltage vector contains the set point for
%   generator (including ref bus) buses, and the reference angle of the
%   swing bus, as well as an initial guess for remaining magnitudes and
%   angles. mpopt is a MATPOWER options vector which can be used to
%   set the termination tolerance, maximum number of iterations, and
%   output options (see 'help mpoption' for details). Uses default
%   options if this parameter is not given. Returns the final complex
%   voltages, a flag which indicates whether it converged or not, and
%   the number of iterations performed.

%   MATPOWER Version 2.0
%   by Ray Zimmerman, PSERC Cornell    12/24/97
%   Copyright (c) 1996, 1997 by Power System Engineering Research Center (PSERC)
%   See http://www.pserc.cornell.edu/ for more info.
global max_num_sol
%% default arguments
if nargin < 7
    mpopt = mpoption;
end

%% options
tol		= mpopt.pf.tol;
max_it	= mpopt.pf.nr.max_it;
verbose	= mpopt.verbose;

%% initialize

% V0 = [1.000000000000000 + 0.000000000000000i
%   0.985795245183253 + 0.167951584017804i
%   0.996534978452127 + 0.083174736076643i
%   0.986136280322889 - 0.041445908115392i
%   0.973075428288496 - 0.068338708856769i
%   1.002808831663217 + 0.033715183747089i
%   0.985586887401744 + 0.010692064923283i
%   0.993996114630962 + 0.066005818427047i
%   0.954862527299042 - 0.072633401642287i
% ];
V = V0;

Vorig0 = V0;

Vm_sp = abs(V(pv)); % Voltage control set point for pv buses

buses = [ref;pv;pq];
Ybus = Ybus(buses,:);
Ybus = Ybus(:,buses);

[yy,idx_sort] = sort(buses);

%% set up indexing for updating V
nref = length(ref);
npv	= length(pv);
npq	= length(pq);
nbus = nref + npv + npq;

j1 = 1;         j2 = 2*nref;        %% j1:j2 - V for ref buses
j3 = j2 + 1;	j4 = j2 + 2*npv;	%% j3:j4 - V for pv buses
j5 = j4 + 1;	j6 = j4 + 2*npq;    %% j5:j6 - V for pq buses

rp = 1:2:2*nbus-1; % Indices for real part for all buses
ip = 2:2:2*nbus;   % Indices for imaginary part for all buses
perm(rp) = 1:nbus;
perm(ip) = nbus+(1:nbus);

V = V(buses);
V1(rp,1) = real(V);
V1(ip,1) = imag(V);

%% decompose Ybus to real and imaginary parts and permute the matrix
Ybustemp = [real(Ybus),-imag(Ybus);imag(Ybus),real(Ybus)];
Ybus1 = Ybustemp(perm,:);
Ybus1 = Ybus1(:,perm);

% indices for real and imaginary part of pv and pq bus voltages in the
% voltage vector
rp_pv = j2+(1:2:2*npv);
ip_pv = j2+(2:2:2*npv);
rp_pq = j4+(1:2:2*npq);
ip_pq = j4+(2:2:2*npq);


%% deflation counter
deflation = 0;

max_def = max_num_sol;
%% array of PF solutions found
pfsols = []; %zeros(nbus,max_def);%zeros(nbus,1);
%pfsols([ref;pv]) = abs(V0([ref;pv]));
npfsols = 0;
pfsol_guess = [];
init_guess = pfsols;
alpha = 0.01*eye(2*(npv+npq)); %0.001*ones(length(j1:j6)); %eye(length(j1:j6)) + 0.1*diag([bus([pv;pq],PD);bus(pq,QD)]/baseMVA);
pow = 1.0;
damping_min = 1.5;

Va_variation = [pi,0,-pi];
idx_variation = 0;

while(deflation < max_def)
    V = V0;
    V = V(buses);
    V1(rp,1) = real(V);
    V1(ip,1) = imag(V);
    
    mis = zeros(2*nbus,1);
    
    P_pv = real(Sbus(pv));
    
    %% PV buses
    e_pv = V1(rp_pv);
    f_pv = V1(ip_pv);
    diage_pv = diag(e_pv);
    diagf_pv = diag(f_pv);
    mis(rp_pv) = e_pv.^2 + f_pv.^2 - Vm_sp.^2;
    mis(ip_pv) = diage_pv*Ybus1(rp_pv,:)*V1 + diagf_pv*Ybus1(ip_pv,:)*V1 - P_pv;
    
    %% PQ buses
    QD_pq = imag(Sbus(pq));
    PD_pq = real(Sbus(pq));
    e_pq = V1(rp_pq);
    f_pq = V1(ip_pq);
    diage_pq = diag(e_pq);
    diagf_pq = diag(f_pq);
    
    mis(ip_pq) = diage_pq*Ybus1(rp_pq,:)*V1 + diagf_pq*Ybus1(ip_pq,:)*V1 - PD_pq;
    mis(rp_pq) = diagf_pq*Ybus1(rp_pq,:)*V1 - diage_pq*Ybus1(ip_pq,:)*V1 - QD_pq;
    
    %% deflation operator
    M = deflation_operator(V1,nbus,npv,npq,rp,ip,pfsols,npfsols,pow,alpha);
    
    F_orig = mis(j3:j6);
    
    F = M*F_orig;
    
    %% check tolerance
    normF = norm(F,inf);
    if verbose > 1
        fprintf('\n it    max P & Q mismatch (p.u.)');
        fprintf('\n----  ---------------------------');
        fprintf('\n%3d        %10.3e', ctr, normF);
    end
    if normF < tol
        converged = 1;
        if verbose > 1
            fprintf('\nConverged!\n');
        end
    end
    
    converged = 0;
    ctr = 0;
    
    
    %% do Newton iterations
    while (~converged & ctr < max_it)
        %% update iteration counter
        ctr = ctr + 1;
        
        %% evaluate Jacobian
        delta = 1e-8;
        
        J1 = zeros(2*(npv+npq),2*(npv+npq));
        for s = 1:2*(npv+npq)
            mis = zeros(2*nbus,1);
            V2 = V1;
            V2(j2+s) = V2(j2+s) + delta;
            %% PV buses
            e_pv = V2(rp_pv);
            f_pv = V2(ip_pv);
            diage_pv = diag(e_pv);
            diagf_pv = diag(f_pv);
            mis(rp_pv) = e_pv.^2 + f_pv.^2 - Vm_sp.^2;
            mis(ip_pv) = diage_pv*Ybus1(rp_pv,:)*V2 + diagf_pv*Ybus1(ip_pv,:)*V2 - P_pv;
            
            %% PQ buses
            QD_pq = imag(Sbus(pq));
            PD_pq = real(Sbus(pq));
            e_pq = V2(rp_pq);
            f_pq = V2(ip_pq);
            diage_pq = diag(e_pq);
            diagf_pq = diag(f_pq);
            
            mis(ip_pq) = diage_pq*Ybus1(rp_pq,:)*V2 + diagf_pq*Ybus1(ip_pq,:)*V2 - PD_pq;
            mis(rp_pq) = diagf_pq*Ybus1(rp_pq,:)*V2 - diage_pq*Ybus1(ip_pq,:)*V2 - QD_pq;
            
            %% deflation operator
            M = deflation_operator(V2,nbus,npv,npq,rp,ip,pfsols,npfsols,pow,alpha);
            
            F_orig = mis(j3:j6);
            
            F1 = M*F_orig;
            
            J1(:,s) = (F1 - F)/delta;
            
        end
        
        
        %        J = sparse(2*npv+2*npq,2*npv+2*npq);
        %         %% pv buses
        %         J(rp_pv-j2,rp_pv-j2) = 2*diag(e_pv);
        %         J(rp_pv-j2,ip_pv-j2) = 2*diag(f_pv);
        %
        %         J(ip_pv-j2,:)        = diage_pv*Ybus1(rp_pv,j3:j6) + diagf_pv*Ybus1(ip_pv,j3:j6);
        %         J(ip_pv-j2,rp_pv-j2) = J(ip_pv-j2,rp_pv-j2) + diag(Ybus1(rp_pv,:)*V1);
        %         J(ip_pv-j2,ip_pv-j2) = J(ip_pv-j2,ip_pv-j2) + diag(Ybus1(ip_pv,:)*V1);
        %
        %         %% pq buses
        %         J(rp_pq-j2,:)        = diagf_pq*Ybus1(rp_pq,j3:j6) - diage_pq*Ybus1(ip_pq,j3:j6);
        %         J(rp_pq-j2,rp_pq-j2) = J(rp_pq-j2,rp_pq-j2) - diag(Ybus1(ip_pq,:)*V1);
        %         J(rp_pq-j2,ip_pq-j2) = J(rp_pq-j2,ip_pq-j2) + diag(Ybus1(rp_pq,:)*V1);
        %
        %         J(ip_pq-j2,:)        = diage_pq*Ybus1(rp_pq,j3:j6) + diagf_pq*Ybus1(ip_pq,j3:j6);
        %         J(ip_pq-j2,rp_pq-j2) = J(ip_pq-j2,rp_pq-j2) + diag(Ybus1(rp_pq,:)*V1);
        %         J(ip_pq-j2,ip_pq-j2) = J(ip_pq-j2,ip_pq-j2) + diag(Ybus1(ip_pq,:)*V1);
        
        %% compute update step
        dx = -(J1 \ F);
        
        Fprv = F;
        normFprv = norm(Fprv);
        normF = normFprv + 1.0;
        damping = 2.0;
        V1prv = V1;
        
        while(normF > normFprv && damping > damping_min)
            damping = damping/2.0;
            %% update voltage
            if(~isempty(pv))   %% To avoid error for empty pv vector
                V1(j3:j4) = V1prv(j3:j4) + damping*dx(1:2*npv);
            end
            V1(j5:j6) = V1prv(j5:j6) + damping*dx(2*npv+1:end);
            
            %% PV buses
            e_pv = V1(rp_pv);
            f_pv = V1(ip_pv);
            diage_pv = diag(e_pv);
            diagf_pv = diag(f_pv);
            mis(rp_pv) = e_pv.^2 + f_pv.^2 - Vm_sp.^2;
            mis(ip_pv) = diage_pv*Ybus1(rp_pv,:)*V1 + diagf_pv*Ybus1(ip_pv,:)*V1 - P_pv;
            
            %% PQ buses
            QD_pq = imag(Sbus(pq));
            PD_pq = real(Sbus(pq));
            e_pq = V1(rp_pq);
            f_pq = V1(ip_pq);
            diage_pq = diag(e_pq);
            diagf_pq = diag(f_pq);
            
            mis(ip_pq) = diage_pq*Ybus1(rp_pq,:)*V1 + diagf_pq*Ybus1(ip_pq,:)*V1 - PD_pq;
            mis(rp_pq) = diagf_pq*Ybus1(rp_pq,:)*V1 - diage_pq*Ybus1(ip_pq,:)*V1 - QD_pq;
            
            %% deflation operator
            M = deflation_operator(V1,nbus,npv,npq,rp,ip,pfsols,npfsols,pow,alpha);
            
            F_orig = mis(j3:j6);
            
            F = M*F_orig;
            
            %% check for convergence
            normF = norm(F, inf)
            deflation
        end
        
        if verbose > 1
            fprintf('\n%3d        %10.3e', ctr, normF);
        end
        if norm(F_orig,inf) < tol
            converged = 1;
            if verbose
                fprintf('\nNewton''s method power flow converged in %d iterations.\n', ctr);
            end
        end
    end %% End of Newton loop
    
    if converged
        V = V1(rp) + 1j*V1(ip);
        if deflation>0
            mat=repmat(V,1,deflation);
            diffmat=abs(mat-pfsols);
            diffmat=diffmat(2:end,:);
        else
            diffmat=10;
        end
        
        if ~(sum(diffmat)<0.005)%~(any(all(diffmat<10^-4)))
            deflation = deflation + 1;
            
            pfsol_guess = [pfsol_guess,idx_variation];
            
            norm(F_orig);
            npfsols = npfsols + 1;
            pfsols = [pfsols,V];
            pfsolsrev(pv,1)=V(pv)-2*real(V(pv));pfsolsrev(pq,1)=abs(1-real(V(pq)))+1i*imag(V(pq));
            pfsolsrevim(pv,1)=V(pv)-2*1i*imag(V(pv));pfsolsrevim(pq,1)=real(V(pq))+1i*abs(1-imag(V(pq)));
            pfsolsrevboth(pv,1)=-V(pv);pfsolsrevboth(pq,1)=abs(1-real(V(pq)))+1i*abs(1-imag(V(pq)));
            pfsolsrev(ref,1)=V(ref);pfsolsrevim(ref,1)=V(ref);pfsolsrevboth(ref,1)=V(ref);
            init_guess=[init_guess,V,pfsolsrev,pfsolsrevim,pfsolsrevboth];
            % First initial guess
            ep0 = real(Vorig0); %real(pfsols(:,idx_variation));
            fp0 = imag(Vorig0); %imag(pfsols(:,idx_variation));
            ep0(2:nbus) = ep0(2:nbus) + 0.001;
            fp0(2:nbus) = fp0(2:nbus) + 0.001;
            V0(buses) = ep0 + 1j*fp0;
            
            idx_variation = 0;
        else
            disp('same solution found');
            idx_variation = idx_variation + 1;
            if(idx_variation > 4*npfsols)
                
                    max_def = deflation;
                    fprintf('Found %d solutions\n',npfsols);
                    break;
%                 alpha=alpha+0.01*eye(2*(npv+npq));
%                 idx_variation=1;
%                 if alpha(1,1)>0.1
%                     max_def = deflation;
%                     fprintf('Found %d solutions\n',npfsols);
%                     break;
%                     alpha=0.01*eye(2*(npv+npq));
%                     idx_variation=1;
%                     pow=pow+1;
%                     if pow>2
%                         max_def = deflation;
%                         fprintf('Found %d solutions\n',npfsols);
%                         break;
%                     end
%                 end
            end
            ep0 = real(init_guess(:,idx_variation));%real(pfsols(:,idx_variation));%
            fp0 = imag(init_guess(:,idx_variation));%imag(pfsols(:,idx_variation));%
            ep0(2:nbus) = ep0(2:nbus) + 0.001;
            fp0(2:nbus) = fp0(2:nbus) + 0.001;
            V0(buses) = ep0 + 1j*fp0;
        end
    else
        idx_variation = idx_variation + 1;
        if(idx_variation > 4*npfsols)
            
                                max_def = deflation;
                                fprintf('Found %d solutions\n',npfsols);
                                break;
%             alpha=alpha+0.01*eye(2*(npv+npq));
%             idx_variation=1;
%             if alpha(1,1)>0.1
% %                                 max_def = deflation;
% %                                 fprintf('Found %d solutions\n',npfsols);
% %                                 break;
%                 alpha=0.01*eye(2*(npv+npq));
%                 idx_variation=1;
%                 pow=pow+1;
%                 if pow>2
%                     max_def = deflation;
%                     fprintf('Found %d solutions\n',npfsols);
%                     break;
%                 end
%             end
        end
        ep0 = real(init_guess(:,idx_variation));%real(pfsols(:,idx_variation));%
        fp0 = imag(init_guess(:,idx_variation));%imag(pfsols(:,idx_variation));%
        ep0(2:nbus) = ep0(2:nbus) + 0.001;
        fp0(2:nbus) = fp0(2:nbus) + 0.001;
        V0(buses) = ep0 + 1j*fp0;
    end
    if verbose
        if ~converged
            fprintf('\nNewton''s method power flow did not converge in %d iterations.\n', ctr);
        end
    end
end
% V(buses) = pfsols(:,1);
Vall=pfsols;
end

function M = deflation_operator(x,nbus,npv,npq,rp,ip,pfsols,npfsols,pow,alpha)

M = eye(2*(npv+npq));
diff_V = ones(2*nbus,1);
for j=1:npfsols
    %     temp = zeros(2*nbus,1);
    %     temp(rp) = (x(rp) - real(pfsols(:,j)));
    %     temp(ip) = (x(ip) - imag(pfsols(:,j)));
    %     diff_V = diff_V.*temp;
    diff_V(rp) = (x(rp) - real(pfsols(:,j)));
    diff_V(ip) = (x(ip) - imag(pfsols(:,j)));
    M = M*diag(1./norm(diff_V,inf).^pow);
end
if npfsols
    %    M = M*diag(1./norm(diff_V).^pow);
    M = M + alpha;
end

end