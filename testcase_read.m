function [fb,tb,R,X,B,bustype,Pl,Ql,Pg,Qg,Vsp,NB,nbr]=testcase_read(Sbase,cas)
%Number of buses in the system is stored in variable NB.
%Number of branch records is stored in  the variable nbr

% Read in the IEEE118 Bus System
if(cas==1)
    fid=fopen('pf118bus.dat');
    bus=textscan(fid,'%d%s%s%d%d%d%f%f%f%f%f%f%f%f%f%f%f%f%f%f','headerlines',2);
    branch=textscan(fid,'%d%d%d%d%d%d%f%f%f%d%d%d%d%d%f%f%f%f%f%f%f','headerlines',1);
% Read in the useful data according to the IEEE standard format
% Determine the bus type, whether PQ,PV or slack
    bustype=cell2mat(bus(:,6));
    
% Bus data and convert to p.u.
    Pg=cell2mat(bus(:,11));Qg=cell2mat(bus(:,12));
    Pl=cell2mat(bus(:,9));Ql=cell2mat(bus(:,10));Vsp=cell2mat(bus(:,14));
    Pl=1/Sbase*Pl';Ql=1/Sbase*Ql';Pg=1/Sbase*Pg';Qg=1/Sbase*Qg';

% Branch data
    fb=cell2mat(branch(:,1));tb=cell2mat(branch(:,2));
    R=cell2mat(branch(:,7));X=cell2mat(branch(:,8));B=cell2mat(branch(:,9));

    NB=length(bustype);nbr=length(fb);
end

% Read in the IEEE14 Bus System
if(cas==2)
    fid=fopen('14bus_3.txt');
    bus=textscan(fid,'%d %d %f %f %f %f %f','headerlines',1);
    branch=textscan(fid,'%d %d %f %f %f','headerlines',1);
end

% Read in the Five Bus System in J.Thorp and C.W.Liu
if(cas==3)
    fid=fopen('fivebus.txt');
    bus=textscan(fid,'%d %d %f %f %f %f %f','headerlines',1);
    branch=textscan(fid,'%d %d %f %f %f','headerlines',1);
end

% Read in the Seven Bus System
if(cas==4)
    fid=fopen('sevenbus.txt');
    bus=textscan(fid,'%d %d %f %f %f %f %f','headerlines',1);
    branch=textscan(fid,'%d %d %f %f %f','headerlines',1);
end

% Read in the Five Bus System in B.C.Lesieutre
if(cas==5)
    fid=fopen('fivebus_PV.txt');
    bus=textscan(fid,'%d %d %f %f %f %f %f','headerlines',1);
    branch=textscan(fid,'%d %d %f %f %f','headerlines',1);
end

if(cas==6)
    fid=fopen('43bus.txt');
    bus=textscan(fid,'%d %d %f %f %f %f %f','headerlines',1);
    branch=textscan(fid,'%d %d %f %f %f','headerlines',1);
    bustype=cell2mat(bus(:,2));Pg=cell2mat(bus(:,3));Qg=cell2mat(bus(:,4));
    Pl=cell2mat(bus(:,5));Ql=cell2mat(bus(:,6));Vsp=cell2mat(bus(:,7));
    Pl=1/Sbase*Pl';Ql=1/Sbase*Ql';Pg=1/Sbase*Pg';Qg=1/Sbase*Qg';
    fb=cell2mat(branch(:,1));tb=cell2mat(branch(:,2));
    R=cell2mat(branch(:,3));X=cell2mat(branch(:,4));B=[];
    NB=length(bustype);nbr=length(fb);
end

if(cas==7)
    fid=fopen('sevenbus_ill.txt');
    bus=textscan(fid,'%d %d %f %f %f %f %f','headerlines',1);
    branch=textscan(fid,'%d %d %f %f %f','headerlines',1);
end

if(cas==8)
    fid=fopen('fivebus_noPV.txt');
    bus=textscan(fid,'%d %d %f %f %f %f %f','headerlines',1);
    branch=textscan(fid,'%d %d %f %f %f','headerlines',1);
end

if(cas==9)
    fid=fopen('three_bus.txt');
    bus=textscan(fid,'%d %d %f %f %f %f %f','headerlines',1);
    branch=textscan(fid,'%d %d %f %f %f','headerlines',1);
end

if(cas==10)
    fid=fopen('three_bus_lossless.txt');
    bus=textscan(fid,'%d %d %f %f %f %f %f','headerlines',1);
    branch=textscan(fid,'%d %d %f %f %f','headerlines',1);
end

if (cas==2 || cas==3 || cas==4 || cas==5 || cas==7 || cas==8 || cas==9 || cas==10)
% Read in the useful data according to the IEEE standard format
% Determine the bus type, whether PQ,PV or slack
    bustype=cell2mat(bus(:,2));

% Bus data and convert to p.u.
    Pg=cell2mat(bus(:,3));Qg=cell2mat(bus(:,4));
    Pl=cell2mat(bus(:,5));Ql=cell2mat(bus(:,6));Vsp=cell2mat(bus(:,7));
    Pl=1/Sbase*Pl';Ql=1/Sbase*Ql';Pg=1/Sbase*Pg';Qg=1/Sbase*Qg';
% Branch data
    fb=cell2mat(branch(:,1));tb=cell2mat(branch(:,2));
    R=cell2mat(branch(:,3));X=cell2mat(branch(:,4));B=cell2mat(branch(:,5));
    
    NB=length(bustype);nbr=length(fb);
end