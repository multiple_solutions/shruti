function [V, converged, i, Vall] = newtonpf_deflated_old_all(Ybus, baseMVA, bus, gen, V0, ref, pv, pq, mpopt)
%NEWTONPF  Solves the power flow using a full Newton's method.
%   [V, CONVERGED, I] = NEWTONPF(YBUS, BASEMVA, BUS, GEN, V0, REF, PV, PQ, MPOPT)
%   solves for bus voltages given the full system admittance matrix (for
%   all buses), the initial vector of complex bus voltages, and column vectors with
%   the lists of bus indices for the swing bus, PV buses, and PQ buses,
%   respectively. The bus voltage vector contains the set point for
%   generator (including ref bus) buses, and the reference angle of the
%   swing bus, as well as an initial guess for remaining magnitudes and
%   angles. MPOPT is a MATPOWER options struct which can be used to
%   set the termination tolerance, maximum number of iterations, and
%   output options (see MPOPTION for details). Uses default options if
%   this parameter is not given. Returns the final complex voltages, a
%   flag which indicates whether it converged or not, and the number of
%   iterations performed.
%
%   See also RUNPF.

%   MATPOWER
%   Copyright (c) 1996-2015 by Power System Engineering Research Center (PSERC)
%   by Ray Zimmerman, PSERC Cornell
%
%   $Id: newtonpf.m 2644 2015-03-11 19:34:22Z ray $
%
%   This file is part of MATPOWER.
%   Covered by the 3-clause BSD License (see LICENSE file for details).
%   See http://www.pserc.cornell.edu/matpower/ for more info.
%
%  03/20/2015: Shrirang Abhyankar - Using input argument baseMVA, bus,gen instead of Sbus to support ZIP load model. Sbus needs to be calculated at each iteration as the loads could be voltage dependent.

global max_num_sol
[PQ, PV, REF, NONE, BUS_I, BUS_TYPE, PD, QD, GS, BS, BUS_AREA, VM, ...
    VA, BASE_KV, ZONE, VMAX, VMIN, LAM_P, LAM_Q, MU_VMAX, MU_VMIN] = idx_bus;

%% default arguments
if nargin < 7
    mpopt = mpoption;
end

nbus = size(bus,1);

%% options
tol     = mpopt.pf.tol;
max_it  = mpopt.pf.nr.max_it;

V = V0;

Vorig0 = V0;

Vm_sp = abs(V(pv)); % Voltage control set point for pv buses
buses = [ref;pv;pq];
Ybus = Ybus(buses,:);
Ybus = Ybus(:,buses);

[yy,idx_sort] = sort(buses);


%% set up indexing for updating V
npv = length(pv);
npq = length(pq);
j1 = 1;         j2 = npv;           %% j1:j2 - V angle of pv buses
j3 = j2 + 1;    j4 = j2 + npq;      %% j3:j4 - V angle of pq buses
j5 = j4 + 1;    j6 = j4 + npq;      %% j5:j6 - V mag of pq buses

%% flag to run deflation loop
deflation = 0;

%% array of PF solutions found
pfsols = [];%zeros(nbus,1);
pfsol_guess = []; %% Initial guess for the current solution
%pfsols([ref;pv]) = abs(V0([ref;pv]));
npfsols = size(pfsols,2);

max_def = max_num_sol;
npfsols = 0;
pfsol_guess = [];
init_guess = pfsols;
alpha = 0.01*eye(length(j1:j6)); %0.001*ones(length(j1:j6)); %eye(length(j1:j6)) + 0.1*diag([bus([pv;pq],PD);bus(pq,QD)]/baseMVA);
pow_Va = 1.0;
pow_Vm = 0.1;
damping_min = 0.1;

Va_variation = [pi,0,-pi];
idx_variation = 1;
idx_Vavariation = 1;

while(deflation < max_def)
    %% initialize
    converged = 0;
    i = 0;
    V = V0;
    Va = angle(V);
    Vm = abs(V);
    
    %% evaluate Sbus
    bus(:,VM) = Vm;
    Sbus = makeSbus(baseMVA,bus,gen,mpopt);
    
    %% evaluate F(x0)
    mis = V .* conj(Ybus * V) - Sbus;
    F_orig = [   real(mis([pv; pq]));
        imag(mis(pq))   ];
    
    %% Build deflation operator
    M = deflation_operator(j1,j6,npv,npq,npfsols,pv,pq,Va,pfsols,Vm,pow_Va,pow_Vm,alpha);
    
    %% deflate the mismatch
    F = M*F_orig;
    
    %% check tolerance
    normF = norm(F, inf);
    if mpopt.verbose > 1
        fprintf('\n it    max P & Q mismatch (p.u.)');
        fprintf('\n----  ---------------------------');
        fprintf('\n%3d        %10.3e', i, normF);
    end
    if normF < tol
        converged = 1;
        if mpopt.verbose > 1
            fprintf('\nConverged!\n');
        end
    end
    
    %% do Newton iterations
    while (~converged && i < max_it)
        %% update iteration counter
        i = i + 1;
        
        %% evaluate Jacobian
        %         [dSbus_dVm, dSbus_dVa] = dSbus_dV(Ybus, V);
        %         [dSload_dVm, dSload_dVa] = dSload_dV(V, baseMVA, bus, issparse(Ybus),mpopt);
        %
        %         dSbus_dVm = dSbus_dVm + dSload_dVm;
        %         dSbus_dVa = dSbus_dVa + dSload_dVa;
        %
        %         j11 = real(dSbus_dVa([pv; pq], [pv; pq]));
        %         j12 = real(dSbus_dVm([pv; pq], pq));
        %         j21 = imag(dSbus_dVa(pq, [pv; pq]));
        %         j22 = imag(dSbus_dVm(pq, pq));
        %
        %         Mprime = zeros(length(j1:j6));
        %         for j=1:npfsols
        %             temp = -pow*diag(([Va([pv;pq]);Vm(pq)] - [angle(pfsols([pv;pq],j));abs(pfsols(pq,j))]).^-(pow+1));
        %             for k=1:npfsols
        %                 if (j ~= k)
        %                     temp = temp*diag(([Va([pv;pq]);Vm(pq)] - [angle(pfsols([pv;pq],k));abs(pfsols(pq,k))]).^-pow);
        %                 end
        %             end
        %             Mprime = Mprime + temp;
        %         end
        %
        %         Jorig = [   j11 j12;
        %                 j21 j22;    ];
        %
        %         J = (M*Jorig + Mprime*diag(F_orig));
        
        %          %% Finite differenced Jacobian
        delta = 1e-8;
        
        J1 = zeros(npv+2*npq,npv+2*npq);
        idx1 = 1;
        idx2 = 1;
        idx3 = 1;
        for s = 1:npv+2*npq
            xa = Va;
            xm = Vm;
            if s <= npv
                xa(pv(idx1)) = xa(pv(idx1)) + delta;
                idx1 = idx1 + 1;
            elseif s <= npv + npq
                xa(pq(idx2)) = xa(pq(idx2)) + delta;
                idx2 = idx2 + 1;
            else
                xm(pq(idx3)) = xm(pq(idx3)) + delta;
                idx3 = idx3 + 1;
            end
            x = xm.*exp(sqrt(-1)*xa);
            %% evaluate Sbus
            bus(:,VM) = xm;
            Sbus = makeSbus(baseMVA,bus,gen,mpopt);
            
            %% evaluate F(x0)
            mis = x .* conj(Ybus * x) - Sbus;
            F_orig1 = [   real(mis([pv; pq]));
                imag(mis(pq))   ];
            
            %% Build deflation operator
            M = deflation_operator(j1,j6,npv,npq,npfsols,pv,pq,xa,pfsols,xm,pow_Va,pow_Vm,alpha);
            
            %% deflate the mismatch
            F1 = M*F_orig1;
            
            J1(:,s) = (F1 - F)/delta;
            
        end
        
        
        %% compute update step
        dx = -(J1 \ F);
        
        Fprv = norm(F);
        normFprv = norm(Fprv);
        normF = normFprv + 1.0;
        damping = 2.0;
        Vaprv = Va;
        Vmprv = Vm;
        
        while(normF > normFprv && damping > damping_min)
            damping = damping/2.0;
            %% update voltage
            if npv
                Va(pv) = Vaprv(pv) + damping*dx(j1:j2);
            end
            if npq
                Va(pq) = Vaprv(pq) + damping*dx(j3:j4);
                Vm(pq) = Vmprv(pq) + damping*dx(j5:j6);
            end
            %         Vm(pq) = min(Vm(pq),1.2);
            %         Va([pv;pq]) = min(max(-pi,Va([pv;pq])),pi);
            V = Vm .* exp(1j * Va);
            Vm = abs(V);            %% update Vm and Va again in case
            Va = angle(V);          %% we wrapped around with a negative Vm
            
            %% evaluate Sbus
            bus(:,VM) = Vm;
            Sbus = makeSbus(baseMVA,bus,gen,mpopt);
            
            %% evaluate F(x)
            mis = V .* conj(Ybus * V) - Sbus;
            F_orig = [   real(mis([pv; pq]));
                imag(mis(pq))   ];
            
            %% Build deflation operator
            M = deflation_operator(j1,j6,npv,npq,npfsols,pv,pq,Va,pfsols,Vm,pow_Va,pow_Vm,alpha);
            
            %% deflate the mismatch
            F = M*F_orig;
            
            %% check for convergence
            normF = norm(F, inf)
        end
        if mpopt.verbose > 1
            fprintf('\n%3d        %10.3e', i, normF);
        end
        if norm(F_orig,inf) < tol
            converged = 1;
            if mpopt.verbose
                fprintf('\nNewton''s method power flow converged in %d iterations.\n', i);
            end
        end
    end %% End of Newton loop
    if converged
        deflation
        if deflation>0
            mat=repmat(V,1,deflation);
            diffmat=abs(mat-pfsols);
            diffmat=diffmat(2:end,:);
        else
            diffmat=10;
        end
        if ~(sum(diffmat)<2*tol)
            norm(F_orig)
            if(~npfsols)
                pfsol_guess = [pfsol_guess,0];
            else
                pfsol_guess = [pfsol_guess,idx_variation];
            end
            npfsols = npfsols + 1;
            pfsols = [pfsols,V];
            pfsolsrev(pv,1)=V(pv)-2*real(V(pv));pfsolsrevim(pv,1)=V(pv)-2*1i*imag(V(pv));pfsolsrevboth(pv,1)=-V(pv);
            pfsolsrev(pq,1)=V(pq,1);pfsolsrevim(pq,1)=V(pq,1);pfsolsrevboth(pq,1)=V(pq,1);
            pfsolsrev(ref,1)=V(ref);pfsolsrevim(ref,1)=V(ref);pfsolsrevboth(ref,1)=V(ref);
            init_guess=[init_guess,V,pfsolsrev,pfsolsrevim,pfsolsrevboth];
            idx_variation = 0;%1;
            deflation = deflation + 1;
%             Vm0 = abs(pfsols(:,idx_variation));
%             Va0 = angle(pfsols(:,idx_variation));
%             Vm0(pq) = Vm0(pq) + 0.001;
%             Va0([pv;pq]) = Va0([pv;pq]) + 0.001;
            V0 = Vorig0;%Vm0.*exp(1j*Va0);
        else
            idx_variation = idx_variation + 1;
            disp('same solution found');
            if(idx_variation > npfsols)
                max_def = deflation;
                fprintf('Found %d solutions\n',npfsols);
                break;
            end
            Vm0 = abs(pfsols(:,idx_variation));
            Va0 = angle(pfsols(:,idx_variation));
            Vm0(pq) = Vm0(pq) + 0.001;
            Va0([pv;pq]) = Va0([pv;pq]) + 0.001;
            V0 = Vm0.*exp(1j*Va0);
        end
    else
        deflation
        idx_variation = idx_variation + 1;
            if(idx_variation > npfsols)
                max_def = deflation;
                fprintf('Found %d solutions\n',npfsols);
                break;
            end
            Va0([pv;pq]) = angle(pfsols([pv;pq],idx_variation)) + 0.001;
            Vm0(pq) = abs(pfsols(pq,idx_variation)) + 0.001;
            V0 = Vm0.*exp(sqrt(-1)*Va0);
    end
    if mpopt.verbose
        if ~converged
            fprintf('\nNewton''s method power flow did not converge in %d iterations.\n', i);
            Vm0 = abs(V);
            Va0 = angle(V);
            Vm0(pq) = Vm0(pq)/2.0; %Vm0(pq);
            Va0([pv;pq]) = Va0([pv;pq])/2.0; %Va0([pv;pq]);
            V0 = Vm0.*exp(sqrt(-1)*Va0);
        end
    end
end
pfsol_guess;
% V = pfsols(:,1);
Vall=pfsols;

end

function M = deflation_operator(j1,j6,npv,npq,npfsols,pv,pq,Va,pfsols,Vm,pow_Va,pow_Vm,alpha)

M = eye(length(j1:j6));
diff_Va = ones(npv+npq,1);
diff_Vm = ones(npq,1);
for j=1:npfsols
    diff_Va = diff_Va.*(Va([pv;pq]) - angle(pfsols([pv;pq],j)));
    diff_Vm = diff_Vm.*(Vm(pq) - abs(pfsols(pq,j)));
end
diff_V = [diff_Va;diff_Vm];
if npfsols
    M(1:npv+npq,:) = M(1:npv+npq,:)*diag(1./norm(diff_V).^pow_Va);
    M(npv+npq+1:end,:) = M(npv+npq+1:end,:)*diag(1./norm(diff_V).^pow_Vm);
end
M = M + alpha;
end
