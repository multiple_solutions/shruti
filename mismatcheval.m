function Jacob=mismatcheval(f,V0,buses,rp,ip,nbus)

V = V0;
V = V(buses);
V1(rp,1) = real(V);
V1(ip,1) = imag(V);

mis = zeros(2*nbus,1);

P_pv = real(Sbus(pv));

%% PV buses
e_pv = V1(rp_pv);
f_pv = V1(ip_pv);
diage_pv = diag(e_pv);
diagf_pv = diag(f_pv);
mis(rp_pv) = e_pv.^2 + f_pv.^2 - Vm_sp.^2;
mis(ip_pv) = diage_pv*Ybus1(rp_pv,:)*V1 + diagf_pv*Ybus1(ip_pv,:)*V1 - P_pv;

%% PQ buses
QD_pq = imag(Sbus(pq));
PD_pq = real(Sbus(pq));
e_pq = V1(rp_pq);
f_pq = V1(ip_pq);
diage_pq = diag(e_pq);
diagf_pq = diag(f_pq);

mis(ip_pq) = diage_pq*Ybus1(rp_pq,:)*V1 + diagf_pq*Ybus1(ip_pq,:)*V1 - PD_pq;
mis(rp_pq) = diagf_pq*Ybus1(rp_pq,:)*V1 - diage_pq*Ybus1(ip_pq,:)*V1 - QD_pq;

%% deflation operator
M = deflation_operator(V1,nbus,npv,npq,rp,ip,pfsols,npfsols,pow,alpha);

F_orig = mis(j3:j6);
