clear all;
syms x;
Xinit=5;
itermax=100;iter=1;
xsol=Xinit;
M=1;
tol=10^-5;
foriginal=(x-1)*(x+1)*(x-10);
forig=double(subs(foriginal,Xinit));
xsoli=[];
for solnum=1:3
    while iter<=itermax && abs(forig)>tol
        M=1;
        for i=1:length(xsoli)
            M=M/(xsol-xsoli(i));
        end
        f=M*foriginal;
        jacobexp=diff(f);
        Jacob=double(subs(jacobexp,xsol));
        xsol=xsol-1/Jacob*(double(subs(f,xsol)));
        forig=double(subs(foriginal,xsol));
        iter=iter+1;
    end
    if iter>itermax
        disp('fail to converge');
    else
        xsoli(solnum)=xsol;
        M=M/(x-xsoli(solnum));
        Msol(solnum)=1/(x-xsoli(solnum));
        iter=1;forig=10;xsol=Xinit;
    end
end

xplot=-20:0.01:20;
y=(xplot-1).*(xplot+1).*(xplot-10);
plot(xplot,y);
Msolvec=double(subs(Msol(1),xplot));
ydef1=y.*Msolvec;%(xplot+1).*(xplot-10);%
hold on;
plot(xplot,ydef1);
ydef2=ydef1.*double(subs(Msol(2),xplot));%(xplot+1);%
plot(xplot,ydef2);
legend('Original function','Deflated function1','Deflated function 2');
hold on;
plot(xplot,zeros(length(xplot),1),'k');hold on;
plot(zeros(length(-100:100),1),-100:100,'k');
ylim([-40 40]);