
h = gcf;
axesObjs = get(h, 'Children');
dataObjs = get(axesObjs, 'Children');
objTypes = get(dataObjs, 'Type');
xdata = get(dataObjs, 'XData');
ydata = get(dataObjs, 'YData');

scale=transpose(xdata);
numsol_diffinitalphapow=transpose(ydata);

figure();
plot(scale,numsol_old);hold on;
plot(scale,numsol_diffinit);hold on;
plot(scale,numsol_diffinitalpha);hold on;
plot(scale,numsol_diffinitalphapow);hold on;
legend('Original','Modification1','Modification2','Modification3');