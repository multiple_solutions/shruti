function matpower2psse_TS3ph(infile)
%% Converts a file in matpower case format to PSS/E v33 format for TS3ph


[PQ, PV, REF, NONE, BUS_I, BUS_TYPE, PD, QD, GS, BS, BUS_AREA, VM, ...
    VA, BASE_KV, ZONE, VMAX, VMIN, LAM_P, LAM_Q, MU_VMAX, MU_VMIN] = idx_bus;
  
  
[F_BUS, T_BUS, BR_R, BR_X, BR_B, RATE_A, RATE_B, ...
    RATE_C, TAP, SHIFT, BR_STATUS, PF, QF, PT, QT, MU_SF, MU_ST, ...
    ANGMIN, ANGMAX, MU_ANGMIN, MU_ANGMAX] = idx_brch;  
  
outfile = [infile,'_TS3ph.raw'];
mpc = feval(infile);

mpc.branch(:,TAP) = 1.0;
mpc.branch(:,SHIFT) = 0.0;

mpc = runpf(mpc);
mpc = ext2int(mpc);

bus = mpc.bus;
gen = mpc.gen;
branch = mpc.branch;

nbus = size(bus,1);
ngen = size(gen,1);
nbranch = size(branch,1);
fid = fopen(outfile,'w');

%% BaseMVA
fprintf(fid,'0,  100.000\t\t / PSS/E-33.2    SUN, MAR 10 2013  17:41 \n');
fprintf(fid,'\n\n');

%% BUS DATA
for(i=1:nbus)
  bus_name = [num2str(bus(i,BUS_I))];
  if(bus(i,BASE_KV) == 0)
    bus(i,BASE_KV) = 100.0;
  end
  
  fprintf(fid,'%6d,''%-12s'',%3.4f,%d,1,1,1,%10.5f,%10.5f,%7.5f,%7.5f,%7.5f,%7.5f\n',...
          bus(i,BUS_I),bus_name,bus(i,BASE_KV),bus(i,BUS_TYPE),...
          bus(i,VM),bus(i,VA),bus(i,VMAX),bus(i,VMIN),bus(i,VMAX),bus(i,VMIN));
        
end

fprintf(fid,'0 / END OF BUS DATA, BEGIN LOAD DATA\n');

lbus = find(mpc.bus(:,PD)~=0 | mpc.bus(:,QD)~=0);

for(i=1:length(lbus))
  YP = bus(lbus(i),PD)/bus(lbus(i),VM)^2;
  YQ = -bus(lbus(i),QD)/bus(lbus(i),VM)^2;
  
  fprintf(fid,'%6d,''%-2s'',1,1,1, 0.000,  0.000, 0.000, 0.000,%10.8f,%10.8f,  1,1,0\n',...
          bus(lbus(i),BUS_I),num2str(1),YP,YQ);

end
fprintf(fid,'0 / END OF LOAD DATA, BEGIN FIXED SHUNT DATA\n');

shuntbus = find(mpc.bus(:,GS)~=0 | mpc.bus(:,BS)~=0);
for(i=1:length(shuntbus))
  fprintf(fid,'%6d,''%-2s'',1,%9.3f, %9.3f\n',...
    bus(shuntbus(i),BUS_I),num2str(1),bus(shuntbus(i),GS),bus(shuntbus(i),BS));
end
fprintf(fid,'0 / END OF FIXED SHUNT DATA, BEGIN GENERATOR DATA\n');
  

[GEN_BUS, PG, QG, QMAX, QMIN, VG, MBASE, GEN_STATUS, PMAX, PMIN, ...
    MU_PMAX, MU_PMIN, MU_QMAX, MU_QMIN, PC1, PC2, QC1MIN, QC1MAX, ...
    QC2MIN, QC2MAX, RAMP_AGC, RAMP_10, RAMP_30, RAMP_Q, APF] = idx_gen;

gen_id = zeros(ngen,1);
unique_gen = unique(gen(:,GEN_BUS));
for(i=1:length(unique_gen))
   ii = find(unique_gen(i) == gen(:,GEN_BUS));
   gen_id(ii) = (1:length(ii))';
end


%% GENERATOR DATA
 for(i=1:ngen)
   gen(i,MBASE) = gen(i,PG)*3.0;
   if(gen(i,MBASE) < 100)
     gen(i,MBASE) = 100.0;
   end
  gen_name = ['GEN-',num2str(gen(i,GEN_BUS))];
  fprintf(fid,'%7d,''%-2s'',%8.4f,%8.4f,%8.4f,%8.4f,%.5f,%5d,%7.4f,%7.4f,%7.4f,%7.4f,%7.4f,%7.4f,%2d,%8.4f,%7.4f,%8.4f,  1,1.0000\n',...
               gen(i,GEN_BUS),num2str(gen_id(i)),gen(i,PG),gen(i,QG),...
               gen(i,QMAX),gen(i,QMIN),gen(i,VG),0,gen(i,MBASE),...
               0.0,0.237,0.0,0.0,1.0,gen(i,GEN_STATUS),100.0,gen(i,PMAX),gen(i,PMIN));
end
fprintf(fid,'0 / END OF GENERATOR DATA, BEGIN BRANCH DATA\n');

[F_BUS, T_BUS, BR_R, BR_X, BR_B, RATE_A, RATE_B, ...
    RATE_C, TAP, SHIFT, BR_STATUS, PF, QF, PT, QT, MU_SF, MU_ST, ...
    ANGMIN, ANGMAX, MU_ANGMIN, MU_ANGMAX] = idx_brch;
%% BRANCH DATA
for(i=1:nbranch)
  branch_id = ['BL'];
  fprintf(fid,'%7d,%7d,''%-2s'',%8.5f,%8.5f,%8.5f,%5.5f,%5.5f,%5.5f,%3.4f,%3.4f,%3.4f,%3.4f,%2d,1,0.00,1,1.0000\n',...
              branch(i,F_BUS),branch(i,T_BUS),branch_id,...
              branch(i,BR_R),branch(i,BR_X),branch(i,BR_B),...
              branch(i,RATE_A),branch(i,RATE_B),branch(i,RATE_C),...
              0.0,0.0,0.0,0.0,branch(i,BR_STATUS));
              
end
  

fprintf(fid,'0 / END OF BRANCH DATA, BEGIN TRANSFORMER DATA\n');
fprintf(fid,'0 / END OF TRANSFORMER DATA, BEGIN AREA DATA\n');
fprintf(fid,'      1,      0,     0.0,  3.000,''            ''\n');
fprintf(fid,'0 / END OF AREA DATA, BEGIN TWO-TERMINAL DC DATA\n');
fprintf(fid,'0 / END OF TWO-TERMINAL DC DATA, BEGIN VSC DC LINE DATA\n');
fprintf(fid,'0 / END OF VSC DC LINE DATA, BEGIN IMPEDANCE CORRECTION DATA\n');
fprintf(fid,'0 / END OF IMPEDANCE CORRECTION DATA, BEGIN MULTI-TERMINAL DC DATA\n');
fprintf(fid,'0 / END OF MULTI-TERMINAL DC DATA, BEGIN MULTI-SECTION LINE DATA\n');
fprintf(fid,'0 / END OF MULTI-SECTION LINE DATA, BEGIN ZONE DATA\n');
fprintf(fid,'    2,''ZONE_2      ''\n');
fprintf(fid,'0 / END OF ZONE DATA, BEGIN INTER-AREA TRANSFER DATA\n');
fprintf(fid,'    2,    1,''1 '',    0.00\n');
fprintf(fid,'0 / END OF INTER-AREA TRANSFER DATA, BEGIN OWNER DATA\n');
fprintf(fid,'    1,''OWNER_1     ''\n');
fprintf(fid,'0 / END OF OWNER DATA, BEGIN FACTS DEVICE DATA\n');
fprintf(fid,'0 / END OF FACTS DEVICE DATA, BEGING SWITCHED SHUNT DATA\n');
fprintf(fid,'0 / END OF SWITCHED SHUNT DATA, BEGIN GNE DATA\n');
fprintf(fid,'0 / END OF GNE DATA, BEGIN INDUCTION MACHINE DATA\n');
fprintf(fid,'0 / END OF INDUCTION MACHINE DATA\n');
fprintf(fid,'Q\n');

fclose(fid);

dynoutfile = [infile,'_TS3ph.dyr'];
fid = fopen(dynoutfile,'w');
  
gdata = [  
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
9 0.04 1 0.09 5.9 1.3 2 1.9 0.2 0.39 0.237 0.1 0 0
];

ctr = 0;
for(i=1:ngen)
  if(gen(i,PG) > 200 & gen(i,PG) < 400)
    gdata(mod(ctr,14)+1,5:6) = 40*gdata(mod(ctr,14)+1,5);
  elseif(gen(i,PG) > 400 & gen(i,PG) < 600)
    gdata(mod(ctr,14)+1,5:6) = 80*gdata(mod(ctr,14)+1,5);
  elseif(gen(i,PG) > 600)
    gdata(mod(ctr,14)+1,5:6) = 120*gdata(mod(ctr,14)+1,5);
  end
  fprintf(fid,'%7d,''GENROU'',%-2d,%7.3f,%7.3f,%7.3f,%7.3f,%7.3f,%7.3f,%7.3f,%7.3f,%7.3f,%7.3f,%7.3f,%7.3f,%7.3f,%7.3f,/USRWHT\n',...
                           gen(i,GEN_BUS),gen_id(i),gdata(mod(ctr,14)+1,:));
 
                         ctr = ctr + 1;                       
end

fclose(fid);

