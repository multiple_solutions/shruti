clear all;
%% Set parameters
itermax=100; %Maximum number of iterations allowed
tol=10^-4;
delta=10^-8;

%% Initialize
Xinit=5; %Initial estimate of solutions
iter=1;
xsol=Xinit;
M=1;
forig=(xsol-1)*(xsol+1)*(xsol-10); %Initial mismatch for f(x)=(x-1)(x+1)(x-10)
Jacob=3*xsol^2-18*xsol-12; %Jacobian or f'(x)
xsolfound=[];
nsols=0;
alpha=0.01;
%% Search for the 3 roots
for solnum=1:3
    while iter<=itermax && abs(forig)>tol
        M=1;
        for i=1:nsols
            M=M/abs(xsol-xsolfound(i));
        end
        if nsols==1
            M=M+alpha;
        end
        f=M*forig;
        Jacob=M*(3*xsol^2-18*xsol-12);
        xsol=xsol-1/Jacob*(f);
        forig=(xsol-1)*(xsol+1)*(xsol-10);
        iter=iter+1;
    end
    if iter>itermax
        disp('fail to converge');
    else
        xsolfound(solnum)=xsol;
        nsols=nsols+1;
%         M=M/(Xinit-xsoli(solnum));
        iter=1;forig=10;xsol=Xinit;
    end
end